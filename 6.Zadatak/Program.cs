﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            StringChecker stringLowerCaseCheker = new StringLowerCaseChecker();
            StringChecker stringUpperCaseCheker = new StringUpperCaseChecker();
            StringChecker stringDigitChecker = new StringDigitChecker();
            StringChecker stringLenghtCheker = new StringLengthChecker(7);

            stringLowerCaseCheker.SetNext(stringUpperCaseCheker);
            stringUpperCaseCheker.SetNext(stringDigitChecker);
            stringDigitChecker.SetNext(stringLenghtCheker);

            string wrong1 = "nameisname";
            string wrong2 = "NameisName";
            string wrong3 = "Name2";
            string right = "Name2Name";

            Console.WriteLine(stringLowerCaseCheker.Check(wrong1));
            Console.WriteLine(stringLowerCaseCheker.Check(wrong2));
            Console.WriteLine(stringLowerCaseCheker.Check(wrong3));
            Console.WriteLine(stringLowerCaseCheker.Check(right));

        }
    }
}
