﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1.Zadatak
{
    class Program
    {
        private static IAbstractIterator iterator;

        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();
            notebook.AddNote(new Note("Songs", "Unforgiven, Californication, Hotel California"));
            notebook.AddNote(new Note("Movies", "Lord Of The Rings, Wanted, 21 Bridges"));
            notebook.AddNote(new Note("Series", "Friends, Game Of Thrones, The Last Kingdom"));

            iterator = notebook.GetIterator();
            while (iterator.IsDone != true)
            {
                iterator.Current.Show();
                iterator.Next();
                Console.WriteLine();
            }
           
        }
    }
}
