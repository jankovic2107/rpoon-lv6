﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, @"C: \Users\Ivan Jankovic\Desktop\RPOON-Lv6.txt");

            logger.Log("Nesto", MessageType.WARNING);
            fileLogger.Log("Jos nesto", MessageType.INFO);
            fileLogger.Log("Samo to", MessageType.ERROR);
            fileLogger.Log("Gotovo", MessageType.WARNING);
        }
    }
}
